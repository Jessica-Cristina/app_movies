import 'package:flutter/material.dart';
import 'package:my_movies_list/ui/pages/search_page.dart';
import 'package:my_movies_list/ui/pages/tab_pages/main_tab_page.dart';
import 'package:my_movies_list/ui/pages/tab_pages/movies_tab_page.dart';
import 'package:my_movies_list/ui/pages/tab_pages/series_tab_page.dart';

class HomePage extends StatelessWidget {
  static const name = 'home-page';

  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Meu catálogo de filmes e séries'),
          bottom: TabBar(
            tabs: [
              Tab(child: Text('Principal')),
              Tab(child: Text('Filmes')),
              Tab(child: Text('Séries')),
            ],
          ),
          actions: [
            IconButton(
              onPressed: () {
                Navigator.pushNamed(context, SearchPage.name);
              },
              icon: Icon(Icons.search),
            )
          ],
        ),
        body: TabBarView(
          children: [
            MainTabPage(),
            MoviesTabPage(),
            SeriesTabPage(),
          ],
        ),
      ),
    );
  }
}
