import 'package:flutter/material.dart';
import 'package:my_movies_list/ui/pages/home_page.dart';
import 'package:my_movies_list/ui/pages/login_page.dart';
import 'package:my_movies_list/ui/pages/search_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My Movies List',
      debugShowCheckedModeBanner: false,
      initialRoute: LoginPage.name,
      routes: {
        LoginPage.name: (_) => LoginPage(),
        HomePage.name: (_) => HomePage(),
        SearchPage.name: (_) => SearchPage(),
      },
    );
  }
}
